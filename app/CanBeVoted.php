<?php

namespace App;

use App\Vote;
use Collective\Html\HtmlFacade as Html;

trait CanBeVoted
{

    //!agergamos la relacion, un post va a tener varios votos
    public function votes()
    {
        // !morphMany es porque es una relación polimorfica la idea es que este trait lo use cualquier entidad...
        /** en una relación polimorfica hay que estalecer ademas del modelo
         *  en el segundo parámetro el nombre genérico de los objetos relacionados */
        return $this->morphMany(Vote::class, 'votable');
    }

    // ! definimos una relación y luego la cargamos de forma ambiciosa en el momento de ejecutar la consulta SQL en LisPostController.php en el objeto principal
    // Podemos decir que cada post tiene un voto
    public function userVote()
    {
        // es una relacion polimorfica...
        return $this->morphOne(Vote::class, 'votable')
            //distinguimos el voto de un usuario con una condición
            // !se pueden encadenar condiciones a nuestras relaciones
            ->where('user_id', auth()->id()) //si esto devuelve null no sirve
            /** Entonces con withDefault() de Eloquent siempre vamos a obtener
            * un voto a pesar de que el usuario no haya votado. usamos el framework
            * para que nos devuelva si o si un objeto si no da un error*/
            ->withDefault();
    }
    public function getCurrentVoteAttribute()
    {
        // no tiene sentido esperar un voto de alguien que no esta conectado
        if(auth()->check()) {
            //usamos la relación que sabemos nos va a traer un solo voto
            //! debemos primero comprobar si el usuario voto o no, no puede devolver null
            return $this->userVote->vote; // obtenemos el valor de la columna vote
        }
        // de lo contrario retorna null
    }
    // otra propiedad dinamica
    public function getVoteComponentAttribute()
    {
        //solo si el usuario esta conectado puede votar
        if (auth()->check()) {
            return Html::tag('app-vote', '', [
                // pasamos el nombre del modulo sobre el cual queremos votar...
                'module' => $this->getTable(), // posts or comments
                'id' => $this->id,
                'score' => $this->score,
                'vote' => $this->current_vote
            ]);
        }
    }
    public function getVoteFrom(User $user)//recibimos al usuario como un parametro y no dependemos del usuario actual
    {

        // !usamos la relación que cargamos de forma ambiciosa en ListPostController
        //tenemos que comprobar que usuario voto por el post
        return  $this->votes //con los parentesis armamos una consulta, sin ellos trae todos los votos de la DB
            ->where('user_id', $user->id) //filtro por el usuario, no necesito por post porque ya esta implicito en la relación.
            ->value('vote'); //obtenemos el voto...

            //si la variable no es null
            if ($userVote) {
                return $userVote->vote; //retornamos el voto del usuario...
            } // por defecto devuelve null


        /** revisamos el modelo de votos, donde el voto sea igual al id del usuario
         * conectado, y solo queremos obtener el valor de la columna vote.
         */
        /* tenemos que agregar los condicionales si no no funciona...
        return Vote::query()

            ->where('post_id', $this->id) //y para el post actual
            ->value('vote'); //+1, -1, null */
    }
    public function upvote()
    {
        $this->addVote(1);
    }

    public function downvote()
    {
        $this->addVote(-1);
    }

    protected function addVote($amount)
    {
        // Usamos el modelo de Eloquent
        /** Entonces eloquent completa los dos campor de la relación
         * polimórfica, votable_id, votable_type */
        $this->votes()->updateOrCreate(
            ['user_id' => auth()->id()],
            ['vote' => $amount]
        );

        $this->refreshPostScore();

    }

    public function undoVote()
    {
        $this->votes()
            ->where('user_id', auth()->id())
            ->delete();

        /* Vote::where([
            'post_id' => $this->id,
            'user_id' => auth()->id(),
        ])->delete(); */

        $this->refreshPostScore();
    }

    protected function refreshPostScore()
    {
        $this->score  = $this->votes()->sum('vote');;

        /*  $this->score = Vote::query()
            ->where(['post_id' => $this->id])
            ->sum('vote');
        */
        $this->save();
    }
}
