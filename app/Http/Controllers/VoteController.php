<?php

namespace App\Http\Controllers;

class VoteController extends Controller
{
    public function upvote($module, $votable) //!lo paso como una variable no como el modelo porque puede ser cualquier cosa...
    {
        // dd($votable);

        $votable->upvote();

        return [
            'new_score' => $votable->score,
        ];
    }

    public function downvote($module, $votable)
    {
        $votable->downvote();

        return [
            'new_score' => $votable->score,
        ];
    }

    public function undoVote($module, $votable)
    {
        $votable->undoVote();

        return [
            'new_score' => $votable->score,
        ];
    }
}
