<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as BaseVerifier;

class VerifyCsrfToken extends BaseVerifier
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [

    ];

    // ! este método es el que se encarga de manejar el midleware...
    // ! lo agregamos, sobreescribimos un método con otro...
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     *
     * @throws \Illuminate\Session\TokenMismatchException
     */
    public function handle($request, Closure $next)
    {
        if ($this->isReading($request) ||
            $this->runningUnitTests() ||
            $this->inExceptArray($request) ||
            $this->tokensMatch($request)) {
            return $this->addCookieToResponse($request, $next($request));
        }

        // !si la petición espera Json
        if ($request->expectsJson()) {
            // vamos a devolver una respuesta en formato Json!!!
            return response()->json(['error' => 'Unauthorized'], 401);
        }

        throw new TokenMismatchException;
    }
}
