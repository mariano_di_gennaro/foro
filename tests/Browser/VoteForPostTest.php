<?php

namespace Tests\Browser;

use Tests\DuskTestCase;
use Laravel\Dusk\Browser;
use Illuminate\Foundation\Testing\DatabaseMigrations;

class VoteForPostTest extends DuskTestCase
{
    use DatabaseMigrations;

    public function test_a_user_can_vote_for_a_post()
    {
        $user = $this->defaultUser(); // necesitamos un usuario

        $post = $this->createPost(); //creamos un post...

        $this->browse(function (Browser $browser) use ($user, $post) {
            $browser->loginAs($user)
                ->visit($post->url) //visitmos la pagina del post...
                //! esta petición la hacemos via Ajax...
                ->pressAndWaitFor('+1') //queremos esperar hasta que el botón este activo...

                /** pausamos la prueba 2 segundos porque se ejecuta muy
                 * rapido y nunca estamos desactibando el boton...*/
                // sleep(2);

                //tenemos una puntiacion = 1 en el elemento con clase current-vote
                ->assertSeeIn('.current-score', 1);

                $this->assertDatabaseHas('posts', [
                    'id' => $post->id,
                    'score' => 1,
                ]);
        });

        $this->assertSame(1, $post->getVoteFrom($user));

    }
}
