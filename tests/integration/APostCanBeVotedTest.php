<?php

use App\Vote;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class APostCanBeVotedTest extends TestCase
{
    use DatabaseTransactions;

    protected $user;
    protected $post;

    function setUp()
    {
        parent::setUp();

        $this->actingAs($this->user = $this->defaultUser());

        $this->post = $this->createPost();
    }

    function test_a_post_can_be_upvoted()
    {
        $this->post->upvote();

        // esto reemplaza a la que sigue...
        $this->assertSame(1, $this->post->current_vote);

       /*  $this->assertDatabaseHas('votes', [
            'post_id' => $this->post->id,
            'user_id' => $this->user->id,
            'vote' => 1,
        ]); */

        $this->assertSame(1, $this->post->score);
    }

    function test_a_post_cannot_be_upvoted_twice_by_the_same_user()
    {
        $this->post->upvote();

        $this->post->upvote();

        $this->assertSame(1, Vote::count());

        $this->assertSame(1, $this->post->score);
    }

    function test_a_post_can_be_downvoted()
    {
        $this->post->downvote();

        $this->assertSame(-1, $this->post->current_vote); //obtenemos el voto

        // $this->assertDatabaseHas('votes', [
        //     'post_id' => $this->post->id,
        //     'user_id' => $this->user->id,
        //     'vote' => -1,
        // ]);

        $this->assertSame(-1, $this->post->score);
    }

    function test_a_post_cannot_be_downvoted_twice_by_the_same_user()
    {
        $this->post->downvote();

        $this->post->downvote();

        $this->assertSame(1, Vote::count());

        $this->assertSame(-1, $this->post->score);
    }

    function test_a_user_can_switch_from_upvote_to_downvote()
    {
        $this->post->upvote();

        $this->post->downvote();

        $this->assertSame(1, Vote::count());

        $this->assertSame(-1, $this->post->score);
    }

    function test_a_user_can_switch_from_downvote_to_upvote()
    {
        $this->post->downvote();

        $this->post->upvote();

        $this->assertSame(1, Vote::count());

        $this->assertSame(1, $this->post->score);
    }

    function test_the_post_score_is_calculated_correctly()
    {
        //! usamos la relación votes
        $this->post->votes()->create([
            'user_id' => $this->anyone()->id,
            'vote' => 1,
        ]); //votable_id, votable_type

        $this->post->upvote();

        $this->assertSame(2, Vote::count());

        $this->assertSame(2, $this->post->score);
    }

    function test_a_post_can_be_unvoted()
    {
        $this->assertNull($this->post->current_vote);

        $this->post->upvote();

        $this->assertSame(1, Vote::count());

        $this->post->undoVote();

        $this->assertNull($this->post->current_vote);

        $this->assertSame(0,$this->post->score);
    }
    function test_get_vote_from_user_returns_the_vote_from_the_right_post()
    {
      $this->post->upvote(); //votamos por ese Post...

      $anotherPost = $this->createPost(); //creamos otro post...

      $this->assertSame(1, $this->post->current_vote); //tenemos el voto en el primero

      $this->assertNull($anotherPost->current_vote); //pero no en el segundo
    }
}
