<?php

// Routes that require authentication.
Route::post('logout', 'Auth\LoginController@logout');

// Posts
Route::get('posts/create', [
    'uses' => 'CreatePostController@create',
    'as' => 'posts.create',
]);

Route::post('posts/create', [
    'uses' => 'CreatePostController@store',
    'as' => 'posts.store',
]);

// Votes
//! estas rutas sirven para todos los módulos que quieran ser votados...

Route::pattern('module', '[a-z]+'); // donde module es una cadena de texto...

//decimos que queremos atar el parametro votable a la siguiente lógica...
Route::bind('votable', function ($votableId, $route) { //1)el id del modulo 2) el objeto de rutas...

    $modules = [
        'posts' => \App\Post::class, //utilizar class devuelve una cadena con el nombre de la clase
        'comments' => \App\Comment::class
    ];

    abort_unless($model = $modules[$route->parameter('module')] ?? null, 404); //aborta a menos que existe el modelo

    // abort_if($model == null, 404); //este es un helper de Laravel que le pasamos algo que pueda dar verdadero o falso
    return $model::findOrFail($votableId);

    /* switch ($route->parameter('module')){
        case 'posts':
            return \App\Post::findOrFail($votableId); //que laravel arroje un 404 si el post no es encontrado...
        case 'comments':
            return \App\Comment::findOrFail($votableId);
        default:
            abort(404); //lanzamos un error 404 porque el modulo no es valido
    }; */
});
Route::post('{module}/{votable}/vote/1', [
    'uses' => 'VoteController@upvote'
]);

Route::post('{module}/{votable}/vote/-1', [
    'uses' => 'VoteController@downvote'
]);

Route::delete('{module}/{votable}/vote', [
    'uses' => 'VoteController@undoVote'
]);

// Comments
Route::post('posts/{post}/comment', [
    'uses' => 'CommentController@store',
    'as' => 'comments.store',
]);

Route::post('comments/{comment}/accept', [
    'uses' => 'CommentController@accept',
    'as' => 'comments.accept',
]);

// Subscriptions
Route::post('posts/{post}/subscribe', [
    'uses' => 'SubscriptionController@subscribe',
    'as' => 'posts.subscribe'
]);

Route::delete('posts/{post}/subscribe', [
    'uses' => 'SubscriptionController@unsubscribe',
    'as' => 'posts.unsubscribe'
]);

Route::get('mis-posts/{category?}', [
    'uses' => 'ListPostController',
    'as' => 'posts.mine',
]);
