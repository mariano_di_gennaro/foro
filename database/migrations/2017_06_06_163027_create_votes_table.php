<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVotesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('votes', function (Blueprint $table) {
            $table->increments('id');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');


            // votable . Trabajamos con relaciones polimórficas
            $table->unsignedInteger('votable_id')->index(); //esta columna va a ser un indice...
            //debemos indicar el tipo de la relación...
            $table->string('votable_type', 20)->index(); //20 son 20 caracteres para poner el tipo...
            // !combinamos los dos anteriores, los id se pueden repetir pero no la
            //! combinación de estos dos. Es necesario pasar el user_id como una
            //! restricción, para que un usuario no pueda votar dos veces...
            $table->unique(['user_id','votable_id', 'votable_type']); //lo pasamos con un array



            $table->tinyInteger('vote');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('votes');
    }
}
